// import React from "react";
import styled from "styled-components";

export default styled.div`
  position: absolute;
  left: -30%;
  width: 150px;
  height: 200px;
  top: 0;

  background: linear-gradient(90deg, #f6d365 0%, #fda085 100%);
  opacity: 1;
  filter: blur(100px);
`;
