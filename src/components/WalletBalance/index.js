import React from "react";
import styled from "styled-components";

import H1 from "../H1";
import P1 from "../P1";

const Container = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: flex-end;
  margin-top: 43px;
`;

function WalletBalance(props) {
  return (
    <Container>
      <H1 fontfamily="SFProText" lineheight="48">
        {props.balance.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
      </H1>
      <P1 size="16" color=" rgba(189, 189, 189, 1)" lineheight="24" PadLeft="7">
        APT
      </P1>
    </Container>
  );
}

export default WalletBalance;
