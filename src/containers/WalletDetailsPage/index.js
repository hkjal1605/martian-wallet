import React, { useState, useEffect } from "react";
import styled from "styled-components";

import { getBalance, airdrop } from "../../api/account";

import WalletBalance from "../../components/WalletBalance";
import WalletBalanceButtonGroup from "../../components/WalletBalanceButtonGroup";
import WalletHeader from "../../components/WalletHeader";
import ChainBalanceDisplay from "../../components/ChainBalanceDisplay";

import USDCIcon from "../../icons/usdc.png";
import USDTIcon from "../../icons/usdt.png";
import CoinsIcon from "../../icons/coins.png";
import Option1Icon from "../../icons/option1.png";
import Option2Icon from "../../icons/option2.png";
import Option3Icon from "../../icons/option3.png";
import Option4Icon from "../../icons/option4.png";
import P1 from "../../components/P1";
import IconBox from "../../components/IconBox";
import WalletBalanceFooter from "../../components/WalletBalanceFooter";
import InfoBar from "../../components/InfoBar";
import SendTokenPage from "../SendTokenPage";

import { useAuth } from "../../context/auth";

const Container = styled.div`
  width: 360px;
  height: 573px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
  border: 2px solid #eee;
  box-sizing: border-box;
  position: relative;
`;

const ChainBalanceContainer = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  aling-items: center;
  justify-content: center;
  padding: 0 20px;
  gap: 14px;
  box-sizing: border-box;
`;

const AddBalanceContainer = styled.div`
  width: 100%;
  display: flex;
  aling-items: center;
  justify-content: center;
  gap: 5px;
  box-sizing: border-box;
  cursor: pointer;
  margin-bottom: 24px;
`;

function WalletDetailsPage() {
  const { account } = useAuth();

  const [currentPage, setCurrentPage] = useState("0");
  const [selectedOption, setSelectedOption] = useState("option1");
  const [balance, setBalance] = useState(0);
  const [ShowInfo, setShowInfo] = useState(null);
  const [isLoading, setIsLoading] = useState(false);

  const ChainBalanceArray = [
    {
      icon: USDCIcon,
      name: "Aptos",
      balance: "5268",
    },
    {
      icon: USDCIcon,
      name: "USDC",
      balance: "1242",
    },
    {
      icon: USDTIcon,
      name: "USDT",
      balance: "1422",
    },
  ];

  const FooterOptions = [
    {
      icon: Option1Icon,
      iconHeight: "21.62px",
      iconWidth: "24px",
      selected: selectedOption === "option1",
      key: "option1",
      onClick: () => setSelectedOption("option1"),
    },
    {
      icon: Option2Icon,
      iconHeight: "20px",
      iconWidth: "20px",
      selected: selectedOption === "option2",
      key: "option2",
      onClick: () => setSelectedOption("option2"),
    },
    {
      icon: Option3Icon,
      iconHeight: "24px",
      iconWidth: "23.4px",
      selected: selectedOption === "option3",
      key: "option3",
      onClick: () => setSelectedOption("option3"),
    },
    {
      icon: Option4Icon,
      iconHeight: "22.76px",
      iconWidth: "24px",
      selected: selectedOption === "option4",
      key: "option4",
      onClick: () => setSelectedOption("option4"),
    },
  ];

  useEffect(() => {
    getBalance(account.address)
      .then((res) => {
        setBalance(res);
      })
      .catch((err) => {
        console.log({ err });
      });
  }, [isLoading, account, currentPage]);

  const handleDeposit = async () => {
    setIsLoading(true);

    airdrop(account.address, 5000)
      .then((res) => {
        setShowInfo(["Airdrop success of 5000 APTOS coins", "success"]);
        setTimeout(() => {
          setShowInfo(null);
        }, 2000);
      })
      .catch((e) => {
        console.error(e);
        setShowInfo(["Airdrop of 5000 APTOS coins Failed", "error"]);
        setTimeout(() => {
          setShowInfo(null);
        }, 2000);
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  return (
    <Container>
      <InfoBar ShowInfo={ShowInfo} />
      <WalletHeader />
      {currentPage === "0" ? (
        <div
          style={{
            width: "100%",
            height: "100%",
            display: "flex",
            flexDirection: "column",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <WalletBalance balance={balance} />
          <WalletBalanceButtonGroup
            handleDeposit={handleDeposit}
            isHandleDepositLoading={isLoading}
            setCurrentPage={setCurrentPage}
          />
          <ChainBalanceContainer>
            {ChainBalanceArray.map((obj) => (
              <ChainBalanceDisplay {...obj} key={obj.name} />
            ))}
          </ChainBalanceContainer>
          <AddBalanceContainer>
            <IconBox src={CoinsIcon} height="16px" width="16px" />
            <P1
              FontWeight="600"
              size="11"
              lineheight="16"
              color="rgba(189, 189, 189, 1)"
            >
              Add Token
            </P1>
          </AddBalanceContainer>
        </div>
      ) : (
        <SendTokenPage
          balance={balance}
          setCurrentPage={setCurrentPage}
          setShowInfo={setShowInfo}
        />
      )}

      <WalletBalanceFooter options={FooterOptions} />
    </Container>
  );
}

export default WalletDetailsPage;
