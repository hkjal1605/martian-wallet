// import React from "react";
import styled from "styled-components";

export default styled.div`
  position: absolute;
  right: -20%;
  width: 150px;
  height: 180px;
  top: 0;

  background: linear-gradient(90deg, #fdcbf1 0%, #fdcbf1 1%, #e6dee9 100%);
  opacity: 1;
  filter: blur(100px);
`;
