import React from "react";
import styled from "styled-components";

import PrimaryButton from "../PrimaryButton";
import ArrowDown from "../../icons/arrow-down.png";
import ArrowUp from "../../icons/arrow-up.png";
import IconBox from "../IconBox";
import Spinner from "../Spinner";

const Container = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  gap: 9px;
  align-items: center;
  padding: 0 21px;
  box-sizing: border-box;
  ${"" /* margin-bottom: 31px; */}
`;

function WalletBalanceButtonGroup(props) {
  return (
    <Container>
      <PrimaryButton
        width="100%"
        iconsize="14"
        height="40px"
        onClick={() => props.handleDeposit()}
      >
        {props.isHandleDepositLoading ? (
          <Spinner marginright="8.67" />
        ) : (
          <IconBox
            src={ArrowDown}
            alt="ArrowDown"
            height="14.67px"
            width="14.67px"
            marginright="8.67px"
          />
        )}
        Airdrop
      </PrimaryButton>
      <PrimaryButton
        width="100%"
        colorbg="#fff"
        border="1px solid #000000"
        iconsize="14"
        iconcolor="black"
        height="40px"
        onClick={() => props.setCurrentPage("1")}
      >
        <IconBox
          src={ArrowUp}
          alt="ArrowUp"
          height="14.67px"
          width="14.67px"
          marginright="8.67px"
        />
        Send
      </PrimaryButton>
    </Container>
  );
}

export default WalletBalanceButtonGroup;
