import "./App.css";
import { AuthProvider } from "./context/auth";
import Routes from "./Routes";

function App() {
  return (
    <AuthProvider>
      <div className="App">
        <Routes />
      </div>
    </AuthProvider>
  );
}

export default App;
