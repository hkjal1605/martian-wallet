import React, { useState } from "react";
import styled from "styled-components";
import { useNavigate } from "react-router-dom";
import { useAuth } from "../../context/auth";
import { getAccountFromMetadata } from "../../api/account";
import lockIcon from "../../icons/lock.png";

import AppLogo from "../../components/AppLogo";
import CustomInput from "../../components/CustomInput";
import H1 from "../../components/H1";
import P1 from "../../components/P1";
import PrimaryButton from "../../components/PrimaryButton";
import IconBox from "../../components/IconBox";
import LeftGradient from "../../components/LeftGradient";
import RightGradient from "../../components/RightGradient";
import Spinner from "../../components/Spinner";
import InfoBar from "../../components/InfoBar";

const Container = styled.div`
  width: 360px;
  height: 573px;
  padding: 25px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  border: 2px solid #eee;
  box-sizing: border-box;
  position: relative;
  overflow: hidden;
`;

function HomePage(props) {
  const navigate = useNavigate();
  const [password, setPassword] = useState("");
  const [ShowInfo, setShowInfo] = useState(null);
  const [isLoading, setLoading] = useState(false);
  const { setAuthAccount } = useAuth();

  const handleSubmit = () => {
    if (!password) {
      console.log("Not entered");
      setShowInfo(["Please Enter a Password", "error"]);
      setTimeout(() => {
        setShowInfo(null);
      }, 2000);
    }

    setLoading(true);

    /* eslint-disable no-undef */
    chrome.storage.local.get(["locked"], async (resp) => {
      try {
        const decrypted = await security.loadMnemonic(password, resp.locked);
        /* eslint-disable no-undef */
        chrome.storage.local.get(
          ["address", "accountMetadata"],
          async (response) => {
            const account = await getAccountFromMetadata(
              decrypted.mnemonic,
              response.accountMetadata[response.address]
            );

            const res = {
              account: account,
              address: response.address,
              code: decrypted.mnemonic,
              name: response.accountMetadata[response.address].name,
            };
            setAuthAccount(res);
            setLoading(false);
            navigate("/wallet-details");
            chrome.runtime.sendMessage({
              channel: "aemc",
              method: "set",
              data: decrypted.mnemonic,
            });
          }
        );
      } catch (err) {
        setLoading(false);
        setShowInfo(["Incorrect password", "error"]);
        setTimeout(() => {
          setShowInfo(null);
        }, 2000);
      }
    });
  };

  return (
    <Container>
      <InfoBar ShowInfo={ShowInfo} loginPage />
      <LeftGradient />
      <RightGradient />
      <AppLogo />
      <H1
        lineheight="27"
        size="41"
        fontweight="700"
        letterspacing="8.24"
        margintop="30.69"
        marginbottom="72"
      >
        MARTIAN
      </H1>
      <CustomInput
        placeholder="Enter your password"
        value={password}
        onChange={(e) => setPassword(e.target.value)}
        type="password"
      />
      <PrimaryButton
        margintop="26px"
        width="100%"
        fontfamily="Inter"
        onClick={handleSubmit}
      >
        {isLoading ? (
          <Spinner />
        ) : (
          <IconBox
            src={lockIcon}
            alt="Lock Icon"
            height="14px"
            width="11px"
            marginright="6px"
          />
        )}
        Unlock
      </PrimaryButton>
      <P1 margintop="100" cursor="pointer" FontWeight="600">
        Forgot Password?
      </P1>
    </Container>
  );
}

export default HomePage;
