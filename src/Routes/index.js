import React from "react";
import { Routes, Route } from "react-router-dom";

import HomePage from "../containers/HomePage";
import SendTokenPage from "../containers/SendTokenPage";
import WalletDetailsPage from "../containers/WalletDetailsPage";

function AppRoutes() {
  const [FinalComponent, setFinalComponent] = React.useState(null);

  chrome.storage.local.get(["OnboardingIsDone"], (resp) => {
    if (resp.OnboardingIsDone) {
      setFinalComponent(
        <Routes>
          <Route path="/" element={<HomePage />} />
          <Route path="/wallet-details" element={<WalletDetailsPage />} />
          <Route path="/send-token" element={<SendTokenPage />} />
        </Routes>
      );
    } else {
      setFinalComponent(
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            justifyContent: "center",
            width: "360px",
            height: "573px",
          }}
        >
          <img src="/images/aptos.svg" alt="aptos" width="72px" height="72px" />
          <h3
            style={{
              margin: "15px 0",
              marginTop: "25px",
              textAlign: "center",
              fontSize: "27px",
            }}
          >
            Please set up your account on
            <span
              className="text-green"
              style={{ cursor: "pointer" }}
              onClick={() => chrome.runtime.sendMessage("open onboarding page")}
            >
              {" "}
              Onboarding page
            </span>
            .
          </h3>
        </div>
      );
    }
  });

  return FinalComponent;
}

export default AppRoutes;
