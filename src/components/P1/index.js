import styled from "styled-components";

export default styled.p`
  letter-spacing: 0.2px;
  font-weight: ${(props) => (props.FontWeight ? props.FontWeight : "400")};
  color: ${(props) => (props.color ? props.color : "#646470")};
  font-size: ${(props) => (props.size ? props.size : "14")}px;
  line-height: ${(props) => (props.lineheight ? props.lineheight : "20")}px;
  margin-top: ${(props) => (props.margintop ? props.margintop : "0")}px;
  margin-right: ${(props) => (props.marginRight ? props.marginRight : "0")}px;
  margin-left: ${(props) => (props.marginleft ? props.marginleft : "0")}px;
  margin-bottom: ${(props) =>
    props.marginbottom ? props.marginbottom : "0"}px;
  text-align: ${(props) => (props.textAlign ? props.textAlign : "initial")};
  padding-right: ${(props) => (props.PadRight ? props.PadRight : "0")}px;
  padding-left: ${(props) => (props.PadLeft ? props.PadLeft : "0")}px;
  cursor: ${(props) => (props.cursor ? props.cursor : "inherit")};
  font-family: ${(props) =>
    props.fontfamily ? props.fontfamily : "SFProText"};
  white-space: initial;
  max-width: 100%;
  font-style: normal;
`;
