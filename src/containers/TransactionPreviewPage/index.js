import React, { useState } from "react";
import styled from "styled-components";

import P1 from "../../components/P1";
import PrimaryButton from "../../components/PrimaryButton";
import getEllipsisTxt from "../../utils/getEllipsisText";
import Spinner from "../../components/Spinner";

import { useAuth } from "../../context/auth";

import { getAccountFromMnemonic, transferAmount } from "../../api/account";

const Container = styled.div`
  width: 360px;
  height: 573px;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  box-sizing: border-box;
  border: 2px solid #eee;
  padding: 20px;
`;

const DetailsContainer = styled.div`
  width: 100%;
  padding: 15px 18px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: flex-start;
  background: #f8f8fa;
  border-radius: 12px;
  box-sizing: border-box;
`;

function TransactionPreviewPage(props) {
  const { account } = useAuth();
  const [isLoading, setIsLoading] = useState(false);

  const handleTransaction = async (userAddress, amount) => {
    setIsLoading(true);

    transferAmount(account.account, userAddress, amount)
      .then((res) => {
        props.setShowInfo([
          `${amount} APT sent successfully`,
          "success",
          `https://explorer.devnet.aptos.dev/account/${userAddress}`,
        ]);
        setTimeout(() => {
          props.setShowInfo(null);
        }, 3000);
      })
      .catch((e) => {
        console.error(e);
        props.setShowInfo([`${amount} APT sending failed`, "error"]);
        setTimeout(() => {
          props.setShowInfo(null);
        }, 2000);
      })
      .finally(() => {
        setIsLoading(false);
        setTimeout(() => {
          props.goToHomePage();
        }, 1000);
      });
  };

  return (
    <Container>
      <P1
        FontWeight="700"
        size="16"
        lineheight="24"
        color="#000000"
        marginbottom="20"
      >
        Summary
      </P1>
      <DetailsContainer>
        <P1 color="#666666" lineheight="18" marginbottom="2">
          From
        </P1>
        <P1 FontWeight="400" marginbottom="16">
          <span style={{ color: "#212121", fontWeight: "600" }}>Wallet 1</span>
          {`(${getEllipsisTxt(account.address)})`}
        </P1>
        <P1 color="#666666" lineheight="18" marginbottom="2">
          To
        </P1>
        <P1 color="212121" FontWeight="600" marginbottom="16">
          {getEllipsisTxt(props.address)}
        </P1>
        <P1 color="#666666" lineheight="18" marginbottom="2">
          Value
        </P1>
        <P1 color="212121" FontWeight="600" marginbottom="16">
          {props.amount} APT
        </P1>
        <P1 color="#666666" lineheight="18" marginbottom="2">
          Transaction Fees
        </P1>
        <P1 color="212121" FontWeight="600">
          {"<0.001 APT"}
        </P1>
      </DetailsContainer>
      <PrimaryButton
        margintop="27px"
        width="100%"
        height="40px"
        iconsize="14"
        onClick={() => handleTransaction(props.address, props.amount)}
      >
        {isLoading ? <Spinner /> : "Confirm and Send"}
      </PrimaryButton>
      <PrimaryButton
        margintop="10px"
        width="100%"
        height="40px"
        iconsize="14"
        iconcolor="#212121"
        colorbg="#fff"
        fontWeight="600"
        onClick={() => props.setCurrentPage("0")}
      >
        Cancel
      </PrimaryButton>
    </Container>
  );
}

export default TransactionPreviewPage;
