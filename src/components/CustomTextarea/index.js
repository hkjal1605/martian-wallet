import React from "react";
import styled from "styled-components";
import P1 from "../P1";

const Container = styled.div`
  width: ${(props) => (props.width ? props.width : "100%")};
  margin-top: ${(props) => (props.margintop ? props.margintop : "0")}px;
  margin-bottom: ${(props) =>
    props.marginbottom ? props.marginbottom : "0"}px;
  margin-left: ${(props) => (props.marginleft ? props.marginleft : "0")}px;
  margin-right: ${(props) => (props.marginright ? props.marginright : "0")}px;
  position: relative;
`;

const TextArea = styled.textarea`
  width: 100%;
  height: 100%;
  padding: 10px 12px;
  outline: none;
  border: none;
  background-color: transparent;
  font-size: 14px;
  line-height: 18px;
  color: #a0a0a8;
  font-family: SFProText;
  box-sizing: border-box;
  border-radius: ${(props) =>
    props.borderradius ? props.borderradius : "30"}px;
  border: ${(props) =>
    props.isValid ? "1px solid #dbdbdb" : "1px solid #DE336A"};
  resize: none;
`;

const AddressValidationText = styled.div`
  position: absolute;
  top: 103px;
  left: 0;
`;

function CustomTextarea(props) {
  return (
    <Container>
      {props.title ? (
        <P1 lineheight="18" color="#646470" marginbottom="2">
          {props.title}
        </P1>
      ) : null}
      <TextArea
        disabled={props.disabled}
        placeholder={props.placeholder}
        type={props.type ? props.type : "text"}
        value={props.value || ""}
        onChange={props.onChange}
        rows={props.rows ? props.rows : 2}
        borderradius={props.borderradius}
        isValid={props.isValid}
      />
      <AddressValidationText>
        <P1
          color={
            props.validationChecking
              ? "#646470"
              : props.isValid
              ? "#2AAA37"
              : "#DE336A"
          }
          lineheight="18"
        >
          {props.validationChecking
            ? "Checking..."
            : props.isValid
            ? "Verified"
            : props.value && "Invalid address"}
        </P1>
      </AddressValidationText>
    </Container>
  );
}

export default CustomTextarea;
