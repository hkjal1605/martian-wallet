import styled, { keyframes } from "styled-components";

const Spin = keyframes`
  0% {
    transform: rotate(0deg);
  }

  100% {
    transform: rotate(360deg);
  }
`;

const Spinner = styled.div`
  display: inline;
  border: 3px solid rgba(0, 0, 0);
  border-radius: 50%;
  border-top: 3px solid #fff;
  border-left: 3px solid #fff;
  margin-left: ${(props) => (props.marginleft ? props.marginleft : "0")}px;
  margin-right: ${(props) => (props.marginright ? props.marginright : "0")}px;
  width: 12px;
  height: 12px;
  -webkit-animation: ${Spin} 0.6s linear infinite;
  animation: ${Spin} 0.6s linear infinite;
`;

export default Spinner;
