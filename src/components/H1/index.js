import styled from "styled-components";

export default styled.h1`
  line-height: ${(props) => (props.lineheight ? props.lineheight : "38")}px;
  font-weight: ${(props) => (props.fontweight ? props.fontweight : "600")};
  letter-spacing: ${(props) =>
    props.letterspacing ? props.letterspacing : "0"}px;
  color: ${(props) => (props.color ? props.color : "rgba(0, 0, 0, 1)")};
  font-size: ${(props) => (props.size ? props.size : "48")}px;
  margin-top: ${(props) => (props.margintop ? props.margintop : "0")}px;
  margin-bottom: ${(props) =>
    props.marginbottom ? props.marginbottom : "0"}px;
  text-align: ${(props) =>
    props.textAlign ? props.textAlign : "initial"} !important;
  margin-left: ${(props) => (props.marginleft ? props.marginleft : "0")}px;
  margin-right: ${(props) => (props.marginright ? props.marginright : "0")}px;
  font-family: ${(props) => (props.fontfamily ? props.fontfamily : "Handjet")};
`;
