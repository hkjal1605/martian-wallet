import React from "react";
import styled from "styled-components";
import IconBox from "../IconBox";

const Container = styled.div`
  width: 100%;
  display: flex;
  aling-items: center;
  justify-content: center;
  border-top: 1px solid #e6e6e9;
`;

const OptionBox = styled.div`
  height: 56px;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  border-top: ${(props) => (props.selected ? "1px solid #0A0A14" : "none")};
  background: ${(props) => (props.selected ? "#eee" : "transparent")};
  cursor: pointer;
`;

function WalletBalanceFooter(props) {
  return (
    <Container>
      {props.options.map((obj) => (
        <OptionBox key={obj.key} selected={obj.selected} onClick={obj.onClick}>
          <IconBox
            src={obj.icon}
            alt="Option"
            height={obj.iconHeight}
            width={obj.iconWidth}
            cursor="pointer"
          />
        </OptionBox>
      ))}
    </Container>
  );
}

export default WalletBalanceFooter;
