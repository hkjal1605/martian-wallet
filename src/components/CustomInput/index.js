import React from "react";
import styled from "styled-components";
import P1 from "../P1";

import IconBox from "../IconBox";
import DropdownIcon from "../../icons/dropdown.png";

const Container = styled.div`
  width: ${(props) => (props.width ? props.width : "100%")};
  margin-top: ${(props) => (props.margintop ? props.margintop : "0")}px;
  margin-bottom: ${(props) =>
    props.marginbottom ? props.marginbottom : "0"}px;
  margin-left: ${(props) => (props.marginleft ? props.marginleft : "0")}px;
  margin-right: ${(props) => (props.marginright ? props.marginright : "0")}px;
`;

const InputContainer = styled.div`
  border-radius: ${(props) =>
    props.borderradius ? props.borderradius : "30"}px;
  border: ${(props) =>
    props.invalid ? "1px solid #DE336A" : "1px solid #dbdbdb"};
  overflow: hidden;
  box-sizing: border-box;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: ${(props) => (props.height ? props.height : "48")}px;
  transition: all 0.3s;
`;

const Input = styled.input`
  width: 100%;
  height: 100%;
  padding: 0 20px;
  outline: none;
  border: none;
  background-color: transparent;
  font-size: 14px;
  line-height: 18px;
  color: #a0a0a8;
  font-family: SFProText;
  box-sizing: border-box;

  &[type="number"]::-webkit-inner-spin-button,
  &[type="number"]::-webkit-outer-spin-button {
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    margin: 0;
  }
`;

const SideBar = styled.div`
  width: 45%;
  height: 100%;
  background: #f8f8fa;
  border-left: 1px solid #dbdbdb;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const ValueControls = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  flex-direction: column;
  margin-right: 14px;
`;

function CustomInput(props) {
  return (
    <Container>
      {props.title ? (
        <P1 lineheight="18" color="#646470" marginbottom="2">
          {props.title}
        </P1>
      ) : null}
      <InputContainer
        borderradius={props.borderradius}
        height={props.height}
        invalid={props.invalid}
      >
        <Input
          disabled={props.disabled}
          placeholder={props.placeholder}
          type={props.type ? props.type : "text"}
          value={props.value || ""}
          onChange={props.onChange}
        />
        {props.type === "number" ? (
          <ValueControls>
            <P1
              FontWeight="700"
              size="10"
              marginbottom="-2.5"
              cursor="pointer"
              color="#DADADA"
              onClick={() => props.controlValue(1)}
            >
              &#8896;
            </P1>
            <P1
              FontWeight="700"
              size="10"
              margintop="-2.5"
              cursor="pointer"
              color="#DADADA"
              onClick={() => props.controlValue(-1)}
            >
              &#8897;
            </P1>
          </ValueControls>
        ) : null}
        {props.sideBar ? (
          <SideBar>
            <P1 FontWeight="700" lineheight="18" color="#666666">
              APT
            </P1>
            <IconBox
              src={DropdownIcon}
              alt="dropdown"
              marginleft="4px"
              marginbottom="-2px"
              width="9.55px"
              height="6.25px"
            />
          </SideBar>
        ) : null}
      </InputContainer>
    </Container>
  );
}

export default CustomInput;
