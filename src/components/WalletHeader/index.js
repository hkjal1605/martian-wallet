import React from "react";
import styled from "styled-components";
import { useAuth } from "../../context/auth";

import UserIcon from "../../icons/user.png";
import DownIcon from "../../icons/down-arrow.png";
import P1 from "../P1";

import IconBox from "../IconBox";
import getEllipsisTxt from "../../utils/getEllipsisText";

const Container = styled.div`
  width: 100%;
  padding: 10px 22px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  border-bottom: 1px solid #e6e6e9;
  box-sizing: border-box;
  z-index: 15;
  background: #fff;
`;

const GreenDot = styled.div`
  width: 13px;
  height: 13px;
  background: #2aaa37;
  border-radius: 50%;
`;

function WalletHeader() {
  const { account } = useAuth();

  return (
    <Container>
      <div style={{ display: "flex", alignItems: "center" }}>
        <IconBox
          src={UserIcon}
          alt="User Icon"
          height="26px"
          width="26px"
          marginright="6px"
        />
        <IconBox src={DownIcon} alt="Down" height="5.1px" width="9px" />
      </div>
      <div style={{ display: "flex", alignItems: "center" }}>
        <P1 color="black" FontWeight="600">
          Wallet 1{" "}
        </P1>
        <P1>({getEllipsisTxt(account.address, 5, 4)})</P1>
      </div>
      <GreenDot />
    </Container>
  );
}

export default WalletHeader;
