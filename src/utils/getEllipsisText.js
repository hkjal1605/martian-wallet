const getEllipsisTxt = (str, n = 13, m = 5) => {
  if (str) {
    return `${str.substr(0, n)}...${str.substr(str.length - m, str.length)}`;
  }
  return "";
};

export default getEllipsisTxt;
