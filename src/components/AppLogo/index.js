import React from "react";
import logo from "../../icons/logo.png";

import styled from "styled-components";

const LogoImg = styled.img`
  width: 80px;
  height: 80px;
  object-fit: cover;
`;

function AppLogo() {
  return <LogoImg src={logo} alt="Logo" />;
}

export default AppLogo;
