import React from "react";
import IconBox from "../IconBox";

import P1 from "../P1";
import SuccessIcon from "../../icons/success.png";
import ErrorIcon from "../../icons/error.png";

export default function InfoBar({ ShowInfo, loginPage }) {
  return (
    <div
      style={{
        position: "absolute",
        top: ShowInfo
          ? loginPage
            ? "0px"
            : "47px"
          : loginPage
          ? "-65px"
          : "2px",
        left: "0",
        width: "100%",
        height: ShowInfo ? "53px" : "15px",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        zIndex: "10",
        background: "#212121",
        transition: "all 0.3s ease",
      }}
    >
      {ShowInfo ? (
        <IconBox
          src={
            ShowInfo[1] === "success"
              ? SuccessIcon
              : ShowInfo[1] === "error"
              ? ErrorIcon
              : null
          }
          alt="Success"
          height="24px"
          width="24px"
          marginright="12px"
        />
      ) : null}
      <P1 color="#fff" FontWeight="600" PadRight="5">
        {ShowInfo && ShowInfo[0]}
      </P1>
      {ShowInfo && ShowInfo[2] ? (
        <a target="_blank" href={ShowInfo[2]}>
          <P1
            color="#fff"
            FontWeight="600"
            style={{ borderBottom: "1px solid #fff" }}
          >
            View Info
          </P1>
        </a>
      ) : null}
    </div>
  );
}
