import React, { useEffect, useState } from "react";
import styled from "styled-components";
import axios from "axios";

import CustomInput from "../../components/CustomInput";
import CustomTextarea from "../../components/CustomTextarea";
import P1 from "../../components/P1";
import PrimaryButton from "../../components/PrimaryButton";
import TransactionPreviewPage from "../TransactionPreviewPage";

const Container = styled.div`
  width: 360px;
  height: 573px;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  box-sizing: border-box;
  border: 2px solid #eee;
  padding: 20px;
`;

const TotalBalance = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 30px;
  margin-top: 8px;
`;

const MaxButton = styled.div`
  padding: 0 10px;
  cursor: pointer;
  border: 1px solid #0a0a14;
  border-radius: 27px;
`;

function SendTokenPage(props) {
  const [amount, setAmount] = useState(0);
  const [address, setAddress] = useState("");
  const [currentPage, setCurrentPage] = useState("0");
  const [validationChecking, setValidationChecking] = useState(false);
  const [isAddressValid, setIsAddressValid] = useState(false);
  const [isAddressValidAptos, setIsAddressValidAptos] = useState(false);

  function validateInputAddresses(address) {
    return /[0-9A-Fa-f]{6}/g.test(address);
  }

  function validateAddressApiCall(address) {
    setValidationChecking(true);
    axios
      .get(`https://fullnode.devnet.aptoslabs.com/accounts/${address}`)
      .then((res) => {
        setValidationChecking(false);
        setIsAddressValidAptos(true);
      })
      .catch((err) => {
        setValidationChecking(false);
        setIsAddressValidAptos(false);
      });
  }

  useEffect(() => {
    if (isAddressValid) {
      validateAddressApiCall(address);
    } else {
      setIsAddressValidAptos(false);
    }
  }, [isAddressValid]);

  useEffect(() => {
    setIsAddressValid(address.length === 66 && validateInputAddresses(address));
  }, [address]);

  return currentPage === "0" ? (
    <Container>
      <P1
        FontWeight="700"
        size="16"
        lineheight="24"
        color="#000000"
        marginbottom="22"
      >
        Send Token
      </P1>
      <CustomInput
        type="number"
        title="Amount"
        placeholder="Enter Amount"
        borderradius="8"
        sideBar
        height="40"
        value={amount}
        onChange={(e) => setAmount(e.target.value)}
        invalid={amount > props.balance}
        controlValue={(value) => setAmount(Math.max(0, amount + value))}
      />
      <TotalBalance>
        <P1 lineheight="18">
          <span style={{ color: "#A0A0A8" }}>Balance:</span> {props.balance} APT
        </P1>
        <MaxButton>
          <P1
            size="12"
            lineheight="18"
            color="#212121"
            onClick={() => setAmount(props.balance)}
          >
            Max
          </P1>
        </MaxButton>
      </TotalBalance>
      <CustomTextarea
        type="number"
        title="Receiver’s address"
        placeholder="Enter Receiver’s address"
        borderradius="8"
        value={address}
        onChange={(e) => setAddress(e.target.value)}
        isValid={isAddressValidAptos}
        validationChecking={validationChecking}
      />
      <PrimaryButton
        disabled={!amount || amount > props.balance || !isAddressValidAptos}
        margintop="63px"
        width="100%"
        height="40px"
        iconsize="14"
        onClick={() => setCurrentPage("1")}
      >
        Preview
      </PrimaryButton>
      <PrimaryButton
        margintop="10px"
        width="100%"
        height="40px"
        iconsize="14"
        iconcolor="#212121"
        colorbg="#fff"
        fontWeight="600"
        onClick={() => props.setCurrentPage("0")}
      >
        Cancel
      </PrimaryButton>
    </Container>
  ) : (
    <TransactionPreviewPage
      amount={amount}
      address={address}
      setCurrentPage={setCurrentPage}
      goToHomePage={() => props.setCurrentPage("0")}
      setShowInfo={props.setShowInfo}
    />
  );
}

export default SendTokenPage;
