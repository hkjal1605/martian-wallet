import React from "react";
import styled from "styled-components";
import IconBox from "../IconBox";
import P1 from "../P1";

const Container = styled.div`
  width: 100%;
  padding: 11px 15px;
  border: 1px solid #f8f8fa;
  box-shadow: 0px 4px 6px rgba(0, 0, 0, 0.04);
  border-radius: 6px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  box-sizing: border-box;
`;

function ChainBalanceDisplay(props) {
  return (
    <Container>
      <div style={{ display: "flex", alignItems: "center" }}>
        <IconBox
          src={props.icon}
          alt="Blockchain Icon"
          height="33px"
          width="33px"
        />
        <P1
          size="16"
          lineheight="24"
          FontWeight="700"
          color="black"
          PadLeft="12"
        >
          {props.name}
        </P1>
      </div>

      <P1 FontWeight="600" color="black">
        {props.balance}
      </P1>
    </Container>
  );
}

export default ChainBalanceDisplay;
