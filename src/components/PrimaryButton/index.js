import styled from "styled-components";

export default styled.button`
  cursor: ${(props) => (props.disabled ? "not-allowed" : "pointer")};
  background: ${(props) => (props.colorbg ? props.colorbg : "rgb(0, 0, 0)")};
  border-color: ${(props) =>
    props.bordercolor ? props.bordercolor : "transparent"};
  border: ${(props) =>
    props.noborder ? "none" : props.border ? props.border : "1px"};
  padding: ${(props) => (props.$nopadding ? "0" : "5px 20px")};

  height: ${(props) => (props.height ? props.height : "48px")};
  width: ${(props) => (props.width ? props.width : "234px")};
  max-width: ${(props) => (props.maxwidth ? props.maxwidth : "100%")};
  font-weight: ${(props) => (props.fontWeight ? props.fontWeight : "400")};
  border-radius: ${(props) =>
    props.borderradius ? props.borderradius : "500"}px;
  color: ${(props) => (props.iconcolor ? props.iconcolor : "#fff")};
  font-size: ${(props) => (props.iconsize ? props.iconsize : "16")}px;
  font-family: ${(props) =>
    props.fontfamily ? props.fontfamily : "SFProText"};

  text-align: ${(props) => (props.textAlign ? props.textAlign : "center")};
  display: flex;
  align-items: center;
  justify-content: center;
  margin-bottom: ${(props) =>
    props.marginbottom ? props.marginbottom : "0px"};
  margin-top: ${(props) => (props.margintop ? props.margintop : "0px")};
  margin-left: ${(props) => (props.marginleft ? props.marginleft : "0px")};
  margin-right: ${(props) => (props.marginright ? props.marginright : "0px")};
`;
