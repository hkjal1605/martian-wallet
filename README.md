# Name: Harsh Kumar Jha
# Email: hkjal1605@gmail.com

**This is my submission for the frontend developer opportunity.**

**The code is not deployed to any extension marketplace, you will have to run it locally. I will also include the build files in this repo for your ease. You will have to import the build file in the browser extensions management section.**

***Do let me know if you face any trouble running the code. You can also test it locally in browser by running `npm start`***